/**
  ******************************************************************************
  * @file    bsp_traffic_lights.c
  * @author  fire
  * @version V1.0
  * @date    2024-xx-xx
  * @brief   交通灯函数接口
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 STM32 F103 核心板  
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :http://firestm32.taobao.com
  *
  ******************************************************************************
  */
  
#include "traffic_lights/bsp_traffic_lights.h"    
#include "systick/bsp_SysTick.h"

 /**
  * @brief  初始化控制交通灯的IO
  * @param  无
  * @retval 无
  */
void Traffic_Lights_GPIO_Config(void)
{		
		
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef  GPIO_InitStruct;

    /*开启交通灯相关的GPIO外设时钟*/
    RED_LED1_GPIO_CLK_ENABLE();
    YELLOW_LED2_GPIO_CLK_ENABLE();
    GREEN_LED3_GPIO_CLK_ENABLE();
  
    /*选择要控制的GPIO引脚*/															   
    GPIO_InitStruct.Pin = RED_LED1_PIN;	

    /*设置引脚的输出类型为推挽输出*/
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;  

    /*设置引脚为上拉模式*/
    GPIO_InitStruct.Pull  = GPIO_PULLUP;

    /*设置引脚速率为高速 */   
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    /*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
    HAL_GPIO_Init(RED_LED1_GPIO_PORT, &GPIO_InitStruct);	

    /*选择要控制的GPIO引脚*/															   
    GPIO_InitStruct.Pin = YELLOW_LED2_PIN;	
    HAL_GPIO_Init(YELLOW_LED2_GPIO_PORT, &GPIO_InitStruct);	

    /*选择要控制的GPIO引脚*/															   
    GPIO_InitStruct.Pin = GREEN_LED3_PIN;	
    HAL_GPIO_Init(GREEN_LED3_GPIO_PORT, &GPIO_InitStruct);	

    /*关闭所有交通灯*/
    LED_OFF;
}

 /**
  * @brief  交通灯闪烁
  * @param  GPIOx :GPIO端口
  * @param  GPIO_Pin :GPIO引脚号
  * @retval 无
  */
void Traffic_Lights_flashing(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
    int i;
    for(i = 0 ; i < 5 ; i++)
    {
        digitalToggle(GPIOx,GPIO_Pin);
        Delay_ms(500);      
    }
}

/*********************************************END OF FILE**********************/
