#ifndef __BSP_TRAFFIC_LIGHTS_H
#define	__BSP_TRAFFIC_LIGHTS_H

#include "stm32f1xx.h"

//引脚定义
/*******************************************************/

#define RED_LED1_PIN                      GPIO_PIN_7               
#define RED_LED1_GPIO_PORT                GPIOA                    
#define RED_LED1_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOA_CLK_ENABLE()


#define YELLOW_LED2_PIN                   GPIO_PIN_6              
#define YELLOW_LED2_GPIO_PORT             GPIOA                      
#define YELLOW_LED2_GPIO_CLK_ENABLE()     __HAL_RCC_GPIOA_CLK_ENABLE()

#define GREEN_LED3_PIN                    GPIO_PIN_4               
#define GREEN_LED3_GPIO_PORT              GPIOA                    
#define GREEN_LED3_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()

/************************************************************/

/* 带参宏，可以像内联函数一样使用 */
#define RED_LED1(a) 	HAL_GPIO_WritePin(RED_LED1_GPIO_PORT,RED_LED1_PIN,a)


#define YELLOW_LED2(a)	HAL_GPIO_WritePin(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_PIN,a)

#define GREEN_LED3(a)	HAL_GPIO_WritePin(GREEN_LED3_GPIO_PORT,GREEN_LED3_PIN,a)

/* 直接操作寄存器的方法控制IO */
#define	digitalHi(p,i)			{p->BSRR=i;}			  //设置为高电平		
#define digitalLo(p,i)			{p->BSRR=(uint32_t)i << 16;}				//输出低电平
#define digitalToggle(p,i)		{p->ODR ^=i;}			//输出反转状态


/* 定义控制IO的宏 */
#define RED_LED1_TOGGLE	    	digitalToggle(RED_LED1_GPIO_PORT,RED_LED1_PIN)
#define RED_LED1_OFF	    	digitalLo(RED_LED1_GPIO_PORT,RED_LED1_PIN)
#define RED_LED1_ON			    digitalHi(RED_LED1_GPIO_PORT,RED_LED1_PIN)

#define YELLOW_LED2_TOGGLE		digitalToggle(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_PIN)
#define YELLOW_LED2_OFF		    digitalLo(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_PIN)
#define YELLOW_LED2_ON			digitalHi(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_PIN)

#define GREEN_LED3_TOGGLE		digitalToggle(GREEN_LED3_GPIO_PORT,GREEN_LED3_PIN)
#define GREEN_LED3_OFF		    digitalLo(GREEN_LED3_GPIO_PORT,GREEN_LED3_PIN)
#define GREEN_LED3_ON			digitalHi(GREEN_LED3_GPIO_PORT,GREEN_LED3_PIN)

//红
#define LED_RED  \
					RED_LED1_ON;\
					YELLOW_LED2_OFF\
					GREEN_LED3_OFF

//黄
#define LED_YELLOW		\
					RED_LED1_OFF;\
					YELLOW_LED2_ON\
					GREEN_LED3_OFF

//绿
#define LED_GREEN	\
					RED_LED1_OFF;\
					YELLOW_LED2_OFF\
					GREEN_LED3_ON
                    
//黑(全部关闭)
#define LED_OFF	\
					RED_LED1_OFF;\
					YELLOW_LED2_OFF\
					GREEN_LED3_OFF				

void Traffic_Lights_GPIO_Config(void);
void Traffic_Lights_flashing(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

#endif /* __BSP_TRAFFIC_LIGHTS_H */
