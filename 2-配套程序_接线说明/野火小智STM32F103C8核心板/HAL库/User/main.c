/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2024-xx-xx
  * @brief   交通灯模拟实验例程
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火小智 STM32F103C8 核心板
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx.h"
#include "led/bsp_led.h" 
#include "usart/bsp_debug_usart.h"
#include "key/bsp_key.h"
#include "traffic_lights/bsp_traffic_lights.h"  
#include "systick/bsp_SysTick.h"


/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
int main(void)
{       
    /* 设定系统时钟为72MHz */
    SystemClock_Config();

    /* 启动系统滴答定时器 */
    SysTick_Init();
    
    /* 配置串口1为：115200 8-N-1 */
    DEBUG_USART_Config();
    
	/* 初始化交通灯使用的GPIO */
	Traffic_Lights_GPIO_Config();
    
    printf("\r\n 欢迎使用野火小智F103C8T6核心板 \r\n");
    printf("\r\n 这是一个交通灯模拟实验例程\r\n");
 
    while(1)
    {
        
        LED_GREEN;
        Delay_ms(30000);//此处修改绿灯时间
        Traffic_Lights_flashing(GREEN_LED3_GPIO_PORT,GREEN_LED3_PIN);
        
        LED_YELLOW;
        Delay_ms(3000);
        
        LED_RED;
        Delay_ms(30000);//此处修改红灯时间
        Traffic_Lights_flashing(RED_LED1_GPIO_PORT,RED_LED1_PIN);        

    }        
}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 72000000
  *            HCLK(Hz)                       = 72000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 1
  *            PLLMUL                         = 9
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV1;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_2)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
