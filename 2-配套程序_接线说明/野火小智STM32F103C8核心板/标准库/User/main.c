 /**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2024-xx-xx
  * @brief   交通灯模拟实验例程
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火小智 STM32F103C8 核心板
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
#include "stm32f10x.h"
#include "usart/bsp_usart.h"
#include "led/bsp_led.h"
#include "key/bsp_key.h"
#include "traffic_lights/bsp_traffic_lights.h"  
#include "SysTick/bsp_SysTick.h"


/*
 * 函数名：main
 * 描述  ：主函数
 * 输入  ：无
 * 输出  ：无
 */
int main(void)
{ 	
    /* 启动系统滴答定时器 */
    SysTick_Init();

    /* 配置串口为：115200 8-N-1 */
    USART_Config();

	/* 初始化交通灯使用的GPIO */
	Traffic_Lights_GPIO_Config();
    
    printf("\r\n 欢迎使用野火小智F103C8T6核心板 \r\n");
    printf("\r\n 这是一个交通灯模拟实验例程\r\n");

    while(1)
    {

        LED_GREEN;
        Delay_ms(30000);//此处修改绿灯时间
        Traffic_Lights_flashing(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN);
        
        LED_YELLOW;
        Delay_ms(3000);
        
        LED_RED;
        Delay_ms(30000);//此处修改红灯时间
        Traffic_Lights_flashing(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN);        

    }
    
}



/*********************************************END OF FILE**********************/
