/**
  ******************************************************************************
  * @file    bsp_traffic_lights.c
  * @author  fire
  * @version V1.0
  * @date    2024-xx-xx
  * @brief   交通灯函数接口
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103 STM32 核心板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */
  
#include "traffic_lights/bsp_traffic_lights.h"   
#include "SysTick/bsp_SysTick.h"

 /**
  * @brief  初始化控制交通灯的IO
  * @param  无
  * @retval 无
  */
void Traffic_Lights_GPIO_Config(void)
{		
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef GPIO_InitStructure;

    /*开启交通灯相关的GPIO外设时钟*/
    RCC_APB2PeriphClockCmd( RED_LED1_GPIO_CLK | YELLOW_LED2_GPIO_CLK | GREEN_LED3_GPIO_CLK, ENABLE);
    /*选择要控制的GPIO引脚*/
    GPIO_InitStructure.GPIO_Pin = RED_LED1_GPIO_PIN;	

    /*设置引脚模式为通用推挽输出*/
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   

    /*设置引脚速率为50MHz */   
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 

    /*调用库函数，初始化GPIO*/
    GPIO_Init(RED_LED1_GPIO_PORT, &GPIO_InitStructure);	
    
    /*选择要控制的GPIO引脚*/
    GPIO_InitStructure.GPIO_Pin = YELLOW_LED2_GPIO_PIN;

    /*调用库函数，初始化GPIO*/
    GPIO_Init(YELLOW_LED2_GPIO_PORT, &GPIO_InitStructure);
    
    /*选择要控制的GPIO引脚*/
    GPIO_InitStructure.GPIO_Pin = GREEN_LED3_GPIO_PIN;

    /*调用库函数，初始化GPIO*/
    GPIO_Init(GREEN_LED3_GPIO_PORT, &GPIO_InitStructure);

    /* 关闭所有交通灯	*/
    LED_OFF;
    
}

 /**
  * @brief  交通灯闪烁
  * @param  GPIOx :GPIO端口
  * @param  GPIO_Pin :GPIO引脚号
  * @retval 无
  */
void Traffic_Lights_flashing(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    int i;
    for(i = 0 ; i < 5 ; i++)
    {
        digitalToggle(GPIOx,GPIO_Pin);
        Delay_ms(500);      
    }
}

/*********************************************END OF FILE**********************/
