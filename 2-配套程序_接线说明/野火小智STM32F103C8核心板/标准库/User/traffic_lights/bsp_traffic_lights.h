#ifndef __BSP_TRAFFIC_LIGHTS_H
#define	__BSP_TRAFFIC_LIGHTS_H


#include "stm32f10x.h"


/* 定义LED连接的GPIO端口, 用户只需要修改下面的代码即可改变控制的LED引脚 */
#define RED_LED1_GPIO_PORT      	GPIOA		                 /* GPIO端口 */
#define RED_LED1_GPIO_CLK 	        RCC_APB2Periph_GPIOA		 /* GPIO端口时钟 */
#define RED_LED1_GPIO_PIN	    	GPIO_Pin_7			        

#define YELLOW_LED2_GPIO_PORT    	GPIOA			             /* GPIO端口 */
#define YELLOW_LED2_GPIO_CLK 	    RCC_APB2Periph_GPIOA	     /* GPIO端口时钟 */
#define YELLOW_LED2_GPIO_PIN		GPIO_Pin_6			        

#define GREEN_LED3_GPIO_PORT    	GPIOA			             /* GPIO端口 */
#define GREEN_LED3_GPIO_CLK 	    RCC_APB2Periph_GPIOA		 /* GPIO端口时钟 */
#define GREEN_LED3_GPIO_PIN		    GPIO_Pin_4	


/* 使用标准的固件库控制IO*/
#define RED_LED1(a)	if (a)	\
					GPIO_SetBits(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN);\
					else		\
					GPIO_ResetBits(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN)

#define YELLOW_LED2(a)	if (a)	\
					GPIO_SetBits(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_GPIO_PIN);\
					else		\
					GPIO_ResetBits(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_GPIO_PIN)

#define GREEN_LED3(a)	if (a)	\
					GPIO_SetBits(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN);\
					else		\
					GPIO_ResetBits(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN)


/* 直接操作寄存器的方法控制IO */
#define	digitalHi(p,i)		 {p->BSRR=i;}	 //输出为高电平		
#define digitalLo(p,i)		 {p->BRR=i;}	 //输出低电平
#define digitalToggle(p,i)   {p->ODR ^=i;}   //输出反转状态


/* 定义控制IO的宏 */
#define RED_LED1_TOGGLE		     digitalToggle(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN)
#define RED_LED1_OFF		     digitalLo(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN)
#define RED_LED1_ON			     digitalHi(RED_LED1_GPIO_PORT,RED_LED1_GPIO_PIN)

#define YELLOW_LED2_TOGGLE		 digitalToggle(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_GPIO_PIN)
#define YELLOW_LED2_OFF		     digitalLo(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_GPIO_PIN)
#define YELLOW_LED2_ON			 digitalHi(YELLOW_LED2_GPIO_PORT,YELLOW_LED2_GPIO_PIN)

#define GREEN_LED3_TOGGLE	     digitalToggle(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN)
#define GREEN_LED3_OFF		     digitalLo(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN)
#define GREEN_LED3_ON			 digitalHi(GREEN_LED3_GPIO_PORT,GREEN_LED3_GPIO_PIN)

//红
#define LED_RED  \
					RED_LED1_ON;\
					YELLOW_LED2_OFF\
					GREEN_LED3_OFF

//黄
#define LED_YELLOW		\
					RED_LED1_OFF;\
					YELLOW_LED2_ON\
					GREEN_LED3_OFF

//绿
#define LED_GREEN	\
					RED_LED1_OFF;\
					YELLOW_LED2_OFF\
					GREEN_LED3_ON
                    
//黑(全部关闭)
#define LED_OFF	\
					RED_LED1_OFF;\
					YELLOW_LED2_OFF\
					GREEN_LED3_OFF
                    
void Traffic_Lights_GPIO_Config(void);
void Traffic_Lights_flashing(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif /* __BSP_TRAFFIC_LIGHTS_H */
