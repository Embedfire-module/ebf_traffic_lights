/*********************************************************************************************/
【*】程序简介

-工程名称：交通灯模拟实验例程
-实验平台：野火小智 STM32 F103C8T6 核心板

使用野火小智交通灯模块模拟交通灯闪烁

/*********************************************************************************************/
【*】实验操作与注意事项：

实验操作
1.核心板掉电情况下，将模块引出脚按照xxx引脚连接说明.xlsx，接到板子对应脚上
2.确保引脚对应接上的情况下，核心板上电，将例程编译成功后，下载到核心板里
3.核心板的USB转串口通过数据线连接电脑，同时确保电脑安装了串口驱动并能识别到核心板的串口
4.电脑端使用串口调试助手，选择电脑与核心板相连的COM口，设置为115200-N-8-1并打开
5.复位核心板，即可接收核心板串口发送给电脑的数据
6.观察到交通灯按照设定的规律闪烁

注意事项
无

/*********************************************************************************************/
【*】引脚分配

交通灯模块 ：
    R   <---> PA7
    Y   <---> PA6
    G   <---> PA4
    GND <---> GND
 
串口（TTL-USB TO UART）：
CH340的收发引脚与STM32的发收引脚相连
	CH340  RXD  <--->  USART1  TX  (PA9)
	CH340  TXD  <--->  USART1  RX  (PA10)


/*********************************************************************************************/
【*】联系我们

-野火官网  :https://embedfire.com
-野火论坛  :http://www.firebbs.cn
-野火天猫  :https://yehuosm.tmall.com
-野火京东  :https://yehuo.jd.com/
-野火资料下载中心 :https://doc.embedfire.com/products/link/

/*********************************************************************************************/
